$(document).ready(()=>{
    let form = $('#doubtform');
    let guardar = $('#guardar');
    form.submit((event) => {event.preventDefault();});
    guardar.click((event)=>{onClickPre("doubts.php?id="+ gup('id') +"");});
    
    function onClickPre(url) {
        let pageform = form.find("#pageform").val().trim();
        let message = $("#message-text").val();
        connection(url, pageform,message);
    }
    
    function connection(url, pageform, message) {
        console.log(url);
        // AJAX connection
        $.ajax({url: url,
                data: {pageform: pageform, message: message},
                method: "POST",
                dataType: "text"})
            .done((data, textStatus, jqXHR) => {
                $('#exampleModalCenter').modal('hide')
            })
            .fail((jqXHR) => {
                console.log(jqXHR);
                alert('This page already has doubts written down');
            });
    }
});