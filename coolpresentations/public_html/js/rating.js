var $star_rating = $('.star-rating .fa');

var SetRatingStar = function() {
  return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
      return $(this).removeClass('o');
    } else {
      return $(this). addClass('o');
    }
  });
};

$star_rating.on('click', function() {
  var myClass = $(this).parent().attr('class');
  var ret = myClass.split(" ");
  var myClass1 = ret[0];
  var myClass2 = ret[1];
  $star_rating=$('.' + myClass1 + '.' + myClass2 + ' .fa');
  $star_rating.siblings('input.rating-value').val($(this).data('rating'));
  return SetRatingStar();
});

SetRatingStar();
$(document).ready(function() {

});

$(document).ready(()=>{
  let form = $('#ratingform');
  let guardar = $('#feedsubmit');
  
  form.submit((event) => {event.preventDefault();});
  guardar.click((event)=>{onClickPre("rating.php?id="+ gup('id') +"");});
  
  function onClickPre(url) {
      let first = 5-$(".star-rating.uno > .o").length;
      let second = 5-$(".star-rating.dos > .o").length;
      let third = 5-$(".star-rating.tres > .o").length;
      let fourth = 5-$(".star-rating.cuatro > .o").length;
      let fifth = 5-$(".star-rating.cinco > .o").length;

      connection(url, first, second, third, fourth, fifth);
  }
  
  function connection(url, first, second, third, fourth, fifth) {
      console.log(url);
      // AJAX connection
      $.ajax({url: url,
              data: {first: first, second: second, third: third, fourth: fourth, fifth: fifth},
              method: "POST",
              dataType: "text"})
          .done((data, textStatus, jqXHR) => {
              guardar.prop("disabled", true);
              $('#RatingModal').modal('hide')
          })
          .fail((jqXHR) => {
              console.log(jqXHR);
              alert('This presentation has already been punctuated');
          });
  }
});
