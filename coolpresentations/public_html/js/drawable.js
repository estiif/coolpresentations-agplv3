              var pixelSize = 10;
              var firstcircle=0;
              var firstsquareX=0;
              var firstsquareY=0;
              var variability=52;
              interact(".rainbow-pixel-canvas")
                .origin("self")
                .draggable({
                  max: Infinity,
                  maxPerElement: Infinity,
                  
                })
                // draw colored squares on move
                .on("dragmove", function(event) {
                  var context = event.target.getContext("2d"),
                    // calculate the angle of the drag direction
                    dragAngle = 180 * Math.atan2(event.dx, event.dy) / Math.PI;
              
                  // set color based on drag angle and speed
                  context.fillStyle =
                    "hsl(" +
                    dragAngle +
                    ", 86%, " +
                    (30 + Math.min(event.speed / 1000, 1) * 50) +
                    "%)";

                  //Calculate first and last
                  if (firstcircle==0){
                    if (firstsquareX==0){
                      firstsquareX=event.pageX - pixelSize / 2;
                      firstsquareY=event.pageY - pixelSize / 2;
                    }
                  }
                  

                  // draw squares
                  context.fillRect(
                    event.pageX - pixelSize / 2,
                    event.pageY - pixelSize / 2,
                    pixelSize,
                    pixelSize
                  );
                })

                // Open Doubts Modal
                .on("dragend", function(event) {
                  if ((firstsquareX >= ((event.pageX - pixelSize / 2)-variability)) && (firstsquareX <= ((event.pageX - pixelSize / 2)+variability))){
                    if ((firstsquareY >= ((event.pageY - pixelSize / 2)-variability)) && (firstsquareY <= ((event.pageY - pixelSize / 2)+variability))){
                      if (firstcircle==0){
                        $("#dudabutton").trigger('click');
                        firstcircle=1;
                      }
                    }
                  }
                })

                

                // clear the canvas on doubletap
                .on("doubletap", function(event) {
                  firstsquareX=0;
                  firstcircle=0;
                  var context = event.target.getContext("2d");
                  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
                });
              
              function resizeCanvases() {
                [].forEach.call(document.querySelectorAll(".rainbow-pixel-canvas"), function(
                  canvas
                ) {
                  canvas.width = document.body.clientWidth;
                  canvas.height = window.innerHeight * 0.7;
                });
              }

              //Clear text on close doubt modal
              $('#exampleModalCenter').on('hidden.bs.modal', function (e) {
                $('#message-text').val('');
              });
              
              // interact.js can also add DOM event listeners
              interact(document).on("DOMContentLoaded", resizeCanvases);
              interact(window).on("resize", resizeCanvases);
              $(".rainbow-pixel-canvas").on('touchstart', function (e) { e.preventDefault(); });

             