
$(document).ready(()=>{
    var pageRendering = false,
    pageNumPending = null,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d'),

    canvas2 = document.getElementById('second-canvas'),
    ctx2 = canvas2.getContext('2d'),
    overSlides = $('#over-slides');

    //DAVID ---------------------------------------------------
    $("body").on("change","input[name=options]:checked",function(){
        var eleccion = $('input[name=options]:checked').val()
        console.log(eleccion)
        if(eleccion == 1){
            $("#over-slides").css({"z-index":3});
        }else if(eleccion == 2){
            $("#over-slides").css({"z-index":5});
        }
     });

     $("body").on("click","#buttonMenu",function(){
        //over-slides
        setTimeout(function(){
            var position = $("#the-canvas").offset();
        //console.log("eeeeoooo",position);
        $("#over-slides").offset({top:position.top,left:position.left});
        $("#over-slides").width($("#the-canvas").width());
        $("#over-slides").height($("#the-canvas").height());
        }, 1000);
        
     })

    // FIN DAVID ------------------------------------------------




    var pwd_ok = false;
    var pdfDoc = null, pageNum = 1;
    var url = "";
    var link = $("#download_btn");
    var pagesurveys=[];
    /**
     * Asynchronously downloads PDF.
     */  
    function loadPdf(access_code) {
        if(!PRES_CODE) {
            alert('Error: no pres_code!');
        }
        url = "presentation_access.php?presentation_code="+encodeURI(PRES_CODE);
        if (PRES.access_code && !access_code) {
            $('#passwordModal').modal('show');
            setTimeout(()=>{$('input[name="pwd_field"]').focus()},500);
            return;
        }
        if(access_code) {
            url += "&access_code="+encodeURI(access_code);
        }
        if (link.length === 1) {
          link.attr('href', url);
        }
        // If absolute URL from the remote server is provided, configure the CORS
        // header on that server
        pdfjsLib.getDocument(url).then(
        (pdfDoc_) => {
          pdfDoc = pdfDoc_;
          pwd_ok = true;
          $('#passwordModal').modal('hide');
          let pageCount = document.getElementById('page_count');
          if(pageCount) pageCount.textContent = pdfDoc.numPages;
          // Initial/first page rendering
          renderPage(pageNum);
        },
        (data, moreData) => {
            $('#passwordModal .loader').hide();
            $('#pass_dialog_fail').show();
            $('#pwd_form').show();
            console.log(data, moreData);
        });
    }
   
    var startX,
    startY,
    dist,
    threshold = $( canvas ).width() / 3.0, //required min distance traveled to be considered swipe
    allowedTime = 500, // maximum time allowed to travel that distance
    elapsedTime,
    startTime;
    
    var currentSurvey = null;
    
    // Object to do the swap on
   /* var doSwapOn = overSlides[0];
    
    doSwapOn.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
        //e.preventDefault()
    }, false)
        
    doSwapOn.addEventListener('touchmove', function(e){
        //e.preventDefault() // prevent scrolling when inside DIV
    }, false)
    
    doSwapOn.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        dist = touchobj.pageX - startX // get total dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        // check that elapsed time is within specified, horizontal dist traveled >= threshold, and vertical dist traveled <= 100
        var swiperightBol = (elapsedTime <= allowedTime && Math.abs(dist) >= threshold && Math.abs(touchobj.pageY - startY) <= 100)
        //console.log("swiperightBol ", swiperightBol, "dist", dist, "elapsedTime", elapsedTime)
        var dir_str = "none";
        var dir_int = 0;
        if(swiperightBol){
            e.preventDefault()
            if(dist > 0){
                dir_str = "RIGHT";
                dir_int = 1;
            }else{
                dir_str = "LEFT";
                dir_int = 2;
            }
            var _e = new CustomEvent("swap", {
                target : event.target,
                detail: {		
                    direction : dir_str,
                    direction_int : dir_int
                },
                bubbles: true,
                cancelable: true
            });
            trigger(event.target,"Swap",_e);			
        }
        
        //handleswipe(swiperightBol, event.target);
    }, false)*/

    function trigger(elem, name, event) {
        elem.dispatchEvent(event);
        eval(elem.getAttribute('on' + name));
    }
    
	// Loaded via <script> tag, create shortcut to access PDF.js exports.
	var pdfjsLib = window['pdfjs-dist/build/pdf'];

	// The workerSrc property shall be specified.
    pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

	/**
	 * Get page info from document, resize canvas accordingly, and render page.
	 * @param num Page number.
	 */
	function renderPage(num) {
	  pageRendering = true;
	  // Using promise to fetch the page
	  pdfDoc.getPage(num).then(function(page) {
        var desiredWidth = $(canvas).width();
        var viewport = page.getViewport(1);
        var scale = desiredWidth / viewport.width;
        //viewport = page.getViewport(scale);
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && scale <=1 ) {
            viewport = page.getViewport(2);
        }else{
            viewport = page.getViewport(scale);
        }
		canvas.height = viewport.height;
        canvas.width = viewport.width;
        canvas2.height = viewport.height;
        canvas2.width=viewport.width;

		// Render PDF page into canvas context
		var renderContext = {
		  canvasContext: ctx,
		  viewport: viewport
		};
        var renderTask = page.render(renderContext);

        
        overSlides.width($(canvas).width());
        overSlides.height($(canvas).height());

        var position = $("#the-canvas").offset();
        $("#second-canvas").position({top:position.top,left:position.left});
        $("#second-canvas").offset({top:position.top,left:position.left});

        document.getElementById("second-canvas").height = $("#the-canvas").height();
        document.getElementById("second-canvas").width = $("#the-canvas").width();
        $("#second-canvas").width($("#the-canvas").width());
        $("#second-canvas").height($("#the-canvas").height());

        //over-slides
        $("#over-slides").offset({top:position.top,left:position.left});
        $("#over-slides").width($("#the-canvas").width());
        $("#over-slides").height($("#the-canvas").height());


        if(SURVEYS[num]) {
            if (pagesurveys.indexOf(num)==-1){
                pagesurveys.push(num);
            }
            //console.log(SURVEYS[num]);
            if(currentSurvey && currentSurvey.drawnAnswers) {
                currentSurvey.drawnAnswers = null;
            }
            currentSurvey = SURVEYS[num];
            // survey callback
            surveyUpdateCallback();
            renderSurvey(currentSurvey);

            if(overSlides.find("#survey").length > 0){
                $("#option-survey").css("display","initial");
                $("#over-slides").css({"z-index":5});
                $("#drag-option").prop("checked", true);
            }


        } else {
            if(currentSurvey && currentSurvey.drawnAnswers) {
                currentSurvey.drawnAnswers = null;
            }
            currentSurvey = null;
            deleteSurvey();
            $("#option-survey").css("display","none");
            $("#over-slides").css("z-index",3);
            $("#draw-option").prop("checked", true);
        }

		// Wait for rendering to finish
		renderTask.promise.then(function() {
		  pageRendering = false;
		  if (pageNumPending !== null) {
			// New page rendering is pending
			renderPage(pageNumPending);
			pageNumPending = null;
		  }
		});
	  });

	  // Update page counters
      document.getElementById('page_num').textContent = num;
      document.getElementById('pageform').value = num;
    }
    
    //use this variable to correct toggle full screen depending on screen
    var mobile=0;
    // Keep the measures of overSlides always right
    $( window ).resize(function(width, height) {
        if($("body").hasClass("fullscreen")) {
            let winW = $(window).width();
            let winH = $(window).height();
            let width = $(canvas).width();
            let height = $(canvas).height();
            // Try to set max height first if width allows it
            if(winH*width/height < winW) {
                $(canvas)
                    .css("height", winH+"px")
                    .css("width",(winH*width/height)+"px");

                $(canvas2)
                    .css("height", winH+"px")
                    .css("width",(winH*width/height)+"px");  
                mobile=0;
            } else {
                $(canvas)
                    .css("height",(winW*height/width)+"px")
                    .css("width", winW+"px");
                $(canvas2)
                    .css("height",(winW*height/width)+"px")
                    .css("width", winW+"px");
                mobile=1;
                    
            }
        }
        overSlides.width($(canvas).width());
        overSlides.height($(canvas).height());
        overSlides.css("box-sizing","content-box");
    });
    
    var surveyTemplate = $('#survey-template').text();
    var surveyAnswerTemplate = $('#survey-answer-template').text();
    var votedSurveyAnswerTemplate = $('#voted-survey-answer-template').text();
    function renderSurvey(survey) {
        let answersHtmlStr = "";
        for(let ans of survey.answers) {
            answersHtmlStr += surveyAnswerTemplate
                .replace("%%VALUE%%", ans.id)
                .replace("%%TEXT%%", ans.text);
        }
        let htmlStr = surveyTemplate
            .replace("%%ANSWERS%%", answersHtmlStr)
            .replace("%%QUESTION%%", survey.question);
        let newElement = $(htmlStr);
        // survey.open survey.multipleChoice
        //newElement.css("left", survey.pos.x + "%");
        //newElement.css("top", survey.pos.y + "%");
        newElement.css("width", survey.size.x + "%");
        //newElement.css("height", survey.size.y + "%");
        // survey answers interaction
        let answerLis = newElement.find("li");
       answerLis.click((event)=>{
            answerLis.removeClass("active");
            $(event.target).addClass("active");
        });
        // survey vote button
        newElement.find('button').click(()=>{
            $.post("poll_vote.php", 
                   {
                       "presentation_code": PRES.id_code,
                       "survey_page": currentSurvey.page,
                       "answer_id": newElement.find('li.active').attr("data-value")
                   },
                   (data)=>{
                       survey.answered = true;
                       surveyUpdateCallback(survey);
                   },
                   'text')
                .fail((data)=>{alert("Error: " + data.responseText);});
        });
        // Put to overslides
        overSlides.empty();
        overSlides.append(newElement);

        var survey = document.getElementById("survey");
        newElement.css("height", survey.offsetHeight + "px");
        newElement.css("width", survey.offsetWidth + "px");
       
    }
    loadPdf();
    function deleteSurvey() {
        overSlides.html("");
    }

	/**
	 * If another page rendering in progress, waits until the rendering is
	 * finised. Otherwise, executes rendering immediately.
	 */
	function queueRenderPage(num) {
	  if (pageRendering) {
		pageNumPending = num;
	  } else {
		renderPage(num);
	  }
	}

	/**
	 * Displays previous page.
	 */
	function onPrevPage() {
        if (lastpage==0){
            if (pageNum <= 1) {
                
                return;
            }
            pageNum--;
            queueRenderPage(pageNum);
        }
	}
	document.getElementById('prev').addEventListener('click', onPrevPage);

    // Listen for orientation changes      
    window.addEventListener("orientationchange", function() {
        window.setTimeout(function() {
            queueRenderPage(pageNum);
        }, 200);        
    }, false);

	/**
	 * Displays next page.
	 */
    var lastpage = 0;
	function onNextPage() {
      if (lastpage==0){
        if (pageNum >= pdfDoc.numPages) {
            removeElement('over-slides');
            removeElement('the-canvas');
            removeElement('second-canvas');
            removeElement('pages');
            document.getElementById("display-none").style.display = "block";
            lastpage=1;
            //Start to load doubt reminder
            connection("doubtsreminder.php?id="+ gup('id') +"");
            return;
        }
	    pageNum++;
        queueRenderPage(pageNum);
      }
    }

    function removeElement(elementId){
        var element = document.getElementById(elementId);
        element.parentNode.removeChild(element);
    }
    
    $('#pwd_form').submit((event)=>{event.preventDefault();$('button[name="send_pwd"]').click();});
    
    $('button[name="send_pwd"]').click(function() {
        pwd = $('input[name="pwd_field"]').val();
        $('input[name="pwd_field"]').val(''); // delete pass from dom!
        $('#pwd_form').hide();
        $('#passwordModal .loader').show();
        $('#pass_dialog_fail').hide();
        // hash pass even before sending (sha.js)
        var shaObj = new jsSHA("SHA-512", "TEXT");
        shaObj.update(pwd);
        pwd = shaObj.getHash("HEX");
        loadPdf(pwd);
    });
    
    document.getElementById('next').addEventListener('click', onNextPage);
    
   /* window.addEventListener("swap", function(event) {
        //console.log("swap");
        if (event.defaultPrevented) {
            return;
        }
        if (event.detail.direction == "RIGHT") {
            onPrevPage()
        }
        else {
            onNextPage();
        }

        event.preventDefault();
    }, true);*/

    /*window.addEventListener("wheel", function(event) {
        if (pwd_ok) 
        {
            if (event.defaultPrevented) {
                return;
            }

            if (event.deltaY > 0) {
                onNextPage()
            }
            else {
                onPrevPage()
            }

            event.preventDefault();
        }
    }, true);*/

    function keydownevents(event){
        if (event.defaultPrevented){
            return; // Do nothing if the event was already processed
        }

        switch(event.key) {
            case "ArrowLeft":
                onPrevPage()
                event.preventDefault()
                break;
            case "ArrowRight":
                onNextPage()
                event.preventDefault()
                break;
            case "ArrowUp":
                onPrevPage()
                event.preventDefault()
                break;
            case "ArrowDown":
                onNextPage()
                event.preventDefault()
                break;
            case "f":
            case "F":
                toggleFullscreen();
                break;
        }
    }
    window.addEventListener("keydown", keydownevents, true);
    
    // Updates the survey information for the displayed survey
    function surveyUpdateCallback() {
        if( !currentSurvey ) {
            return;
        }
        var theSurvey = currentSurvey;
        $.get('survey_info.php',
            {'pres_id': PRES_CODE,
            'page': currentSurvey.page},
            (data) => {
                for(var key in data) {
                    theSurvey[key] = data[key];
                }
                surveyAnswersDraw(theSurvey);
                setTimeout(surveyUpdateCallback, 5000);
            },
            'json');
    }
 
    
    function surveyAnswersDraw(survey) {
        if(currentSurvey == survey) {
            if(survey.answered || !survey.open || PRESENTATOR_MODE) {
                let sum = survey.answers.reduce((prev,next)=>prev+parseInt(next.votes), 0);
                if(!currentSurvey.drawnAnswers) {
                    let surveyFormUl = overSlides.find(".survey ul");
                    overSlides.find("#vote-button").remove();
                    surveyFormUl.empty();
                    let sum = survey.answers.reduce((prev,next)=>prev+parseInt(next.votes), 0);
                    let answers = [];
                    for(let ans of survey.answers) {
                        let percentage = Math.round(parseInt(ans.votes) / sum * 100);
                        if (isNaN(percentage)){
                            percentage=0;
                        }
                        let newAnswer = $(
                            votedSurveyAnswerTemplate
                            .replace("%%VALUE%%", ans.id)
                            .replace("%%VOTES%%", ans.votes)
                            .replace("%%PERCENTAGE%%", percentage)
                            .replace("%%PERCENTAGE%%", percentage)
                            .replace("%%TEXT%%", ans.text));
                        newAnswer.find(".progress-bar").css("width", percentage+"%");
                        answers.push(newAnswer);
                    }
                    currentSurvey.drawnAnswers = answers;
                    surveyFormUl.append(answers);
                } else {
                    for(let i=0; i < survey.answers.length; i+=1) {
                        let ans = survey.answers[i];
                        let li = survey.drawnAnswers[i];
                        let percentage = Math.round(parseInt(ans.votes) / sum * 100);
                        let bar = li.find(".progress-bar");
                        li.find(".progress-bar")
                          .css("width", percentage+"%")
                          .text(percentage+"%")
                          .attr("aria-valuenow", percentage);
                        li.find(".votes").text(ans.votes);
                    }
                }
            }
        }
    }
    
    $("#fs_btn").click(()=>{
        toggleFullscreen();
    });

    
    function toggleFullscreen() {
        let body = $("body");
        if(body.hasClass("fullscreen")) {
            body.removeClass("fullscreen");
             //Estas lineas en desktop funcionan pero en mobile no
             if (mobile==0){
                $("#the-canvas").css("width","50%");
                $("#the-canvas").css("height","100%");
                $("#second-canvas").css("width","50%");
                $("#second-canvas").css("height","100%");
            }
            else{
                
            }
            $('#fullScreenStyles').remove();
            
        } else {
            body.addClass("fullscreen");
            $('body').append('<style id="fullScreenStyles">*{box-sizing: content-box !important;}</style>');
        }
        $(window).resize();
        //david --------full-screen horizontal centrar verticalmente los dos canvas---------------------------------------------------------

        var position = $("#the-canvas").offset();
        $("#second-canvas").offset({top:position.top,left:position.left});
        document.getElementById("second-canvas").height = $("#the-canvas").height();
        document.getElementById("second-canvas").width = $("#the-canvas").width();
        $("#second-canvas").width($("#the-canvas").width());
        $("#second-canvas").height($("#the-canvas").height());
        
        //control de postions dinamico para vertical
        if(window.innerHeight > window.innerWidth){
             $el = $("#the-canvas");
             $("#pages").attr('style', 'top: '+($el.offset().top + $el.outerHeight(true))+'px !important');
             $("#fullscreen-mobile").attr('style', 'top: '+($el.offset().top + $el.outerHeight(true))+'px !important');
        }else if(window.innerHeight < window.innerWidth){
            $el = $("#the-canvas");
            //$("#pages").attr('style', 'top: '+($el.offset().top + $el.outerHeight(true))+'px !important');
            $("pages").removeClass("display-none");
            $("#fullscreen-mobile").attr('style', '');
        }
        
        $("#over-slides").offset({top:position.top,left:position.left});
        $("#over-slides").width($("#the-canvas").width());
        $("#over-slides").height($("#the-canvas").height());
        
    }
    $(window).on("resize orientationchange",function(){
        renderPage(pageNum);
        if(window.orientation == 90 || window.orientation == -90){
            $el = $("#the-canvas");
            $("#pages").attr('style', 'top: '+(($el.offset().top + $el.outerHeight(true))-$("#pages").height())+'px !important');
        $("#fullscreen-mobile").attr('style', 'top: '+(($el.offset().top + $el.outerHeight(true))-$("#pages").height())+'px !important');
        }else{
        $el = $("#the-canvas");
        $("#pages").attr('style', 'top: '+($el.offset().top + $el.outerHeight(true))+'px !important');
        $("#fullscreen-mobile").attr('style', 'top: '+($el.offset().top + $el.outerHeight(true))+'px !important');
        }

    });

    //FIN david -----------------------------------------------------------------
    $("#fullscreen-mobile").click(function(){
        toggleFullscreen();
    })
    //-----------SHOW RESULTS ------------//
    let showed = 0;
    let identificadores = ["primero","segundo","tercero","cuarto","quinto","sexto","septimo","octavo","noveno","decimo"];
    $('#ResultsModal').on('show.bs.modal', function (e) {
        if (showed==0){
            var ResultsQuestionTemplate = $('#results-questions-template').text();
            var ResultsAnswersTemplate = $('#results-answers-template').text();
            for(let k=0; k<pagesurveys.length; k+=1){
                let htmlString ="";
                htmlString = ResultsQuestionTemplate
                .replace("%%QUESTION%%", SURVEYS[pagesurveys[k]].question)
                .replace("%%IDENTIFICADORES%%", identificadores[k])
                .replace("%%IDENTIFICADORES%%", identificadores[k]);
                $('.accordion').append(htmlString);
                let sum = SURVEYS[pagesurveys[k]].answers.reduce((prev,next)=>prev+parseInt(next.votes), 0);
                let resanswers = [];
                for(let i=0; i<SURVEYS[pagesurveys[k]].answers.length; i+=1){
                    let percentage = Math.round(parseInt(SURVEYS[pagesurveys[k]].answers[i].votes) / sum * 100);
                    let htmlStringA = $(ResultsAnswersTemplate 
                        .replace("%%PERCENTAGE%%", percentage)
                        .replace("%%PERCENTAGE%%", percentage)
                        .replace("%%ANSWERS%%", SURVEYS[pagesurveys[k]].answers[i].text));
                    htmlStringA.find(".progress-bar").css("width", percentage+"%");
                    resanswers.push(htmlStringA);
                }
                $('.collapse').last().append(resanswers);
            }  
            showed=1;  
        }   
    });
    
    
    //----------SHOW DOUBT REMINDER --------//
    var showedoubt = 0;
    let doubtresults;
    let clases = ["primary","secondary","success","danger","warning","info","light","dark"];
    
    $('#DoubtsModal').on('show.bs.modal', function (e) {
        //window.addEventListener('keydown', listener, false);
        if (showedoubt==0){
            var ResultsDoubtsTemplate = $('#results-doubts-template').text();
            let m = 0;
            for (let j=0; j<doubtresults.length; j++){
                let htmlstringB = ResultsDoubtsTemplate
                .replace("%%DOUBTS%%",doubtresults[j]['message'])
                .replace("%%PAGES%%",doubtresults[j]['page'])
                .replace("%%CLASES%%",clases[m]);
                $('.list-group.dynamically').last().append(htmlstringB);
                if (m<7){
                    m=m+1;
                }
                else{
                    m=0;
                }
            }
            showedoubt=1;
        }
    });


    function connection(url) {
        
        // AJAX connection
        $.ajax({url: url,
                method: "GET",
                dataType: "json", 
                })
            .done((data, textStatus, jqXHR) => {
                doubtresults=data;
           })
            .fail((jqXHR) => {
                console.log(jqXHR);
                alert('This presentations has no doubts');
            });
    }

    //--------Preventing F toggle full screen on doubtmodal--------//

    $('#exampleModalCenter').on('show.bs.modal', function (e) {
        window.removeEventListener("keydown", keydownevents, true);
    });
    $('#exampleModalCenter').on('hide.bs.modal', function (e) {
        window.addEventListener("keydown", keydownevents, true);
    });


  
    
});