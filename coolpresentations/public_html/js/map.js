//map.js
$('#demo').on('shown.bs.collapse', function () {
    mymap.invalidateSize();
});

var mymap = L.map('mapid').setView([41.38093, 2.182846], 13);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZXN0aWY5NSIsImEiOiJjanRyM2ppbDMwNG1xNDRwMjdmOTkzN241In0.fWrzkA9HwdHF6xVkoXNuRA', {
    //attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZXN0aWY5NSIsImEiOiJjanRyM2ppbDMwNG1xNDRwMjdmOTkzN241In0.fWrzkA9HwdHF6xVkoXNuRA'
}).addTo(mymap);
var mp = new L.Marker([41.38093, 2.182846]);
var firsttime=0;
mymap.on("click", function(e){
        if (firsttime==0){
          mp.setLatLng([e.latlng.lat, e.latlng.lng]).addTo(mymap);
          firsttime=1;
        }
        else{
          mp.setLatLng([e.latlng.lat, e.latlng.lng]);
        }
        document.getElementById('lat').value = e.latlng.lat; //latitude
        document.getElementById('lng').value = e.latlng.lng; //longitude
});

$('#clearmark').on("click", function(e){
    mp.remove();
    firsttime=0;
});
      