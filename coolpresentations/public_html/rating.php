<?php

/*
  This is a ***DEMO*** , the backend / PHP provided is very basic. You can use it as a starting point maybe, but ***do not use this on production***. It doesn't preform any server-side validation, checks, authentication, etc.

  For more read the README.md file on this folder.

  Based on the examples provided on:
  - http://php.net/manual/en/features.file-upload.php

*/
//header('Content-type:application/json;charset=utf-8');



require dirname(__FILE__) . '/../include/database_connection.php';
if ($mysqli->connect_error) {
    http_response_code(500);
    die('Connection error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}


if(!session_id()) session_start();
    
    $code_id=$_GET['id'];
    $first=$_POST['first'];
    $second=$_POST['second'];
    $third=$_POST['third'];
    $fourth=$_POST['fourth'];
    $fifth=$_POST['fifth'];	

function insert_feed($mysqli,$code_id,$first,$second,$third,$fourth,$fifth){
	$stmt = $mysqli->prepare('UPDATE presentations 
    SET firstfeed = firstfeed + ?, secondfeed=secondfeed + ?, thirdfeed=thirdfeed + ?, fourthfeed=fourthfeed + ?, fifthfeed=fifthfeed + ?, votes=votes + 1
    WHERE id_code = ?');
	$stmt->bind_param('iiiiis',$first,$second,$third,$fourth,$fifth,$code_id);
	if(!$stmt->execute()) {
		http_response_code(500);
        $stmt->close();
        $mysqli->close();
        throw new RuntimeException('Error in the query '.$stmt->errno);
    }
    $stmt->close();
}

insert_feed($mysqli,$code_id,$first,$second,$third,$fourth,$fifth);

$mysqli->close();
?>