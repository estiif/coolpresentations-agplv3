<?php

/*
  This is a ***DEMO*** , the backend / PHP provided is very basic. You can use it as a starting point maybe, but ***do not use this on production***. It doesn't preform any server-side validation, checks, authentication, etc.

  For more read the README.md file on this folder.

  Based on the examples provided on:
  - http://php.net/manual/en/features.file-upload.php
*/

header('Content-type:application/json;charset=utf-8');

require dirname(__FILE__) . '/../include/database_connection.php';

if(!session_id()) session_start();

try {
    if (
        !isset($_FILES['file']['error']) ||
        is_array($_FILES['file']['error'])
    ) {
        throw new RuntimeException('Invalid parameters.');
    }
	
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }
	$id_code = hash('sha256', uniqid(rand()));
	$aux = $id_code.'.pdf';
    $filepath = sprintf('../uploaded_pdfs/%s', $aux);

	
	
    if (!move_uploaded_file(
        $_FILES['file']['tmp_name'],
        $filepath
    )) {
        throw new RuntimeException('Failed to move uploaded file.');
    }

    // All good, send the response
	

    echo json_encode([
        'status' => 'ok',
        'path' => $filepath
    ]);
	
	//----------RECOJO JSON----------------//
	
	
	$name=$_POST['present_name'];
	$downloable=$_POST['downloable'];
	$fecha1 = $_POST['diaini'];
	$hora1=$_POST['horaini'];
	$fecha2 = $_POST['diafin'];
	$hora2=$_POST['horafin'];
	$lat=$_POST['lat'];
	$lng=$_POST['lng'];
	$access_code = $_POST['code_access'];
    if($access_code !== "") {
        $access_code = hash('sha512', $access_code); 
        $access_code=strtolower($access_code);
    } else {
        $access_code = null;
    }
	$user_id=$_SESSION['user_id'];
	$start = $fecha1." ".$hora1.":00";
	$fin= $fecha2." ".$hora2.":00";

	// ------- RECOJO DINAMIC ---------- //

	$question = $_POST['question'];
	$page = $_POST['page'];
	$open=1;
	$multiplechoice=$_POST['multiplechoice'];
	$xcor = $_POST['xcor'];
	$ycor = $_POST['ycor'];
	$width = $_POST['width'];
	$height = $_POST['height'];
	$answer1 = $_POST['answer1'];
	$answer2 = $_POST['answer2'];
	$answer3 = $_POST['answer3'];
	$answer4 = $_POST['answer4'];
	$answer5 = $_POST['answer5'];

	//Spliteo las comas de la reocgida dinamica

	$question = explode(',', $question);
	$page = explode(',', $page);
	$multiplechoice = explode(',', $multiplechoice);
	$xcor = explode(',', $xcor);
	$ycor = explode(',', $ycor);
	$width = explode(',', $width);
	$height = explode(',', $height);
	$answer1 = explode(',', $answer1);
	$answer2 = explode(',', $answer2);
	$answer3 = explode(',', $answer3);
	$answer4 = explode(',', $answer4);
	$answer5 = explode(',', $answer5);


	//-------- FIN RECOJO JSON-------------//

} catch (RuntimeException $e) {
	// Something went wrong, send the err message as JSON
	http_response_code(400);

	echo json_encode([
		'status' => 'error',
		'message' => $e->getMessage(),
		'hola' => $_FILES
	]);
	
}




function insert_survey($mysqli,$page,$question,$xcor,$ycor,$open,$multiplechoice,$id_code,$width,$height){
	$stmt = $mysqli->prepare('INSERT INTO surveys VALUES (?,?,?,?,?,?,?,?,?)');
	$stmt->bind_param('isddddiis',$page,$question,$xcor,$ycor,$width,$height,$open,$multiplechoice,$id_code);
	if(!$stmt->execute()) {
		http_response_code(500);
        $stmt->close();
        $mysqli->close();
        throw new RuntimeException('Error in the query '.$stmt->errno);
    }
    $stmt->close();
}

function answer($mysqli,$num,$answer,$id_code,$page){
	$votes=0;
	$stmt = $mysqli->prepare('INSERT INTO survey_answers VALUES (?,?,?,?,?)');
	$stmt->bind_param('isisi',$num,$answer,$votes,$id_code,$page);
	if(!$stmt->execute()) {
		http_response_code(500);
        $stmt->close();
        $mysqli->close();
        throw new RuntimeException('Error in the query '.$stmt->errno);
    }
	$stmt->close();   
}

function insert_presentation($mysqli,$id_code,$name,$start,$fin,$lat,$lon,$access_code,$downloable,$user_id){
	$stmt = $mysqli->prepare('INSERT INTO presentations (id_code,name, start_timestamp, end_timestamp, location_lat, location_lon, access_code, downloadable, user_id) VALUES (?,?,?,?,?,?,?,?,?)');
	$stmt->bind_param('ssssddsii', $id_code,$name,$start,$fin,$lat,$lon,$access_code,$downloable,$user_id);
	if(!$stmt->execute()) {
		http_response_code(500);
        $stmt->close();
        $mysqli->close();
        throw new RuntimeException('Error in the query '.$stmt->errno);
    }
	$stmt->close();
	
}




//----- EJECUTO LOS INSERTS POR ORDEN ----//

insert_presentation($mysqli,$id_code,$name,$start,$fin,$lat,$lng,$access_code,$downloable,$user_id);

foreach ($question as $index => $question){
	if($page[$index]!=0){
		insert_survey($mysqli,$page[$index],$question,$xcor[$index],$ycor[$index],$open,$multiplechoice[$index],$id_code,$width,$height);
	}
}

foreach ($answer1 as $index => $answer1){
	if($answer1!=''){
		$num=1;
		answer($mysqli,$num,$answer1,$id_code,$page[$index]);
	}
}

foreach ($answer2 as $index => $answer2){
	if($answer2!=''){
		$num=2;
		answer($mysqli,$num,$answer2,$id_code,$page[$index]);
	}
}
foreach ($answer3 as $index => $answer3){
	if($answer3!=''){
		$num=3;
		answer($mysqli,$num,$answer3,$id_code,$page[$index]);
	}
}
foreach ($answer4 as $index => $answer4){
	if($answer4!=''){
		$num=4;
		answer($mysqli,$num,$answer4,$id_code,$page[$index]);
	}
}
foreach ($answer5 as $index => $answer5){
	if($answer5!=''){
		$num=5;
		answer($mysqli,$num,$answer5,$id_code,$page[$index]);
	}
}


$mysqli->close();
?>