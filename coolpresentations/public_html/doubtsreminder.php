<?php
require_once dirname(__FILE__) . '/../include/database_connection.php';

function get_doubts_session($paramTypes, $params) {
    global $mysqli;
    $doubts = [];
    $query = "SELECT message,page
            FROM doubts WHERE session_id = ? AND code_id = ?;";
    $stmt = $mysqli->prepare($query);
    if(!$stmt) {
        throw new Exception('Error in the question query preparation ' . $mysqli->error, $mysqli->errno);
    }
    try {
        $params = array_merge([$paramTypes],$params);
        call_user_func_array([$stmt, 'bind_param'], $params);
        if (!$stmt->execute()) {
            throw new Exception('Error in the question query ' . $stmt->errno);
        }
        $stmt->bind_result($message, $page);
        while($stmt->fetch()) {   
            $doubts[] = [
                'message' => $message,'page' => $page                
            ];
        }
    } finally {
        $stmt->close();
    }
    
    /*foreach ($doubts as $doubt){
        echo $doubt['message'];
        echo $doubt['page'];
        echo "\n";
    } */

    header('Content-type:application/json;charset=utf-8');
    echo json_encode($doubts);

    //var_dump($doubts);
    //echo $doubts[0]['message'];
    //echo $doubts[0]['message'];
    return $doubts;
}
if(!session_id()) session_start();
$session_id = session_id();
$presentation_code = $_GET['id'];
$doubts = get_doubts_session('ss', [&$session_id, &$presentation_code]);

