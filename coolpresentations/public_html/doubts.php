<?php

/*
  This is a ***DEMO*** , the backend / PHP provided is very basic. You can use it as a starting point maybe, but ***do not use this on production***. It doesn't preform any server-side validation, checks, authentication, etc.

  For more read the README.md file on this folder.

  Based on the examples provided on:
  - http://php.net/manual/en/features.file-upload.php

*/
//header('Content-type:application/json;charset=utf-8');



require dirname(__FILE__) . '/../include/database_connection.php';
if ($mysqli->connect_error) {
    http_response_code(500);
    die('Connection error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}


if(!session_id()) session_start();


    if (!isset($_SESSION['user_id'])) {
        $user_id=0;
    }
    else{
        $user_id=$_SESSION['user_id'];
    }
    
    $code_id=$_GET['id'];
    $page = $_POST['pageform'];
    $message=$_POST['message'];
    $session_id = session_id();

function insert_doubt($mysqli,$user_id,$code_id,$page,$message,$session_id){
	$stmt = $mysqli->prepare('INSERT INTO doubts (user_id,code_id,page,message,session_id) VALUES (?,?,?,?,?)');
	$stmt->bind_param('isiss',$user_id,$code_id,$page,$message,$session_id);
	if(!$stmt->execute()) {
		http_response_code(500);
        $stmt->close();
        $mysqli->close();
        throw new RuntimeException('Error in the query '.$stmt->errno);
    }
    $stmt->close();
}

insert_doubt($mysqli,$user_id,$code_id,$page,$message,$session_id);

$mysqli->close();
?>