<footer class="bg-dark">
  <div class="container-fluid">
  <div class="row justify-content-center text-center align-items-center">
		<div class="d-none d-lg-block col-lg-4 text-muted">
			<span><strong>Presentations</strong> is an idea of:</span>
			<ul>
				<li><a target="_blank" href="linkedin.com/in/david-campos-rodriguez/">David Campos</a></li>
				<li><a target="_blank" href="#empty">Rubén Bagán</a></li>
				<li><a target="_blank" href="https://www.linkedin.com/in/esteve-oria-garcía-962453123/">Esteve Oria</a></li>
			</ul>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-4 text-muted">
			<span>Licensed under</span>
			<a target="_blank" href="https://www.gnu.org/licenses/agpl-3.0.en.html">Affero GPL 3.0</a>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-4 text-muted">
			<a target="_blank" href="https://bitbucket.org">Source code on BitBucket</a>
		</div>
	</div>
  </div>
</footer>