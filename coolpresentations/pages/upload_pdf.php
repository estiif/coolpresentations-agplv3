<main role="main" class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          
          <!-- Our markup, the important part here! -->
          <div id="drag-and-drop-zone" class="dm-uploader p-5">
            <h3 class="mb-5 mt-5 text-muted">Drag &amp; drop files here</h3>

            <div class="btn btn-primary btn-block mb-5">
                <span>Open the file Browser</span>
                <input type="file" title='Click to add Files' />
            </div>
          </div><!-- /uploader -->

        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card h-100">
            <div class="card-header">
              File List
            </div>

            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
              <li class="text-muted text-center empty">No files uploaded.</li>
            </ul>
          </div>
        </div>
      </div><!-- /file list -->
	 
	 
	 
	<form class="form-horizontal" id="primaryButton" onsubmit="return mySubmitFunction(event)" >
	  <div class="form-group">
			<label >Presentation name:</label>
			<input type="text" class="form-control" id="present_name" >
	  </div>
	  <div class="form-group">
			<label>Access code:</label>
			<input class="form-control" type="text" id="code_access" >
	  </div>
	  <div class="checkbox">
			<label><input id='downloable' type="checkbox"> Downloadable?</label>
	  </div>
	  <div class="form-group">
			<label>Init time:</label>
			<input type="date" id="diaini" class="form-control"  >
			<input type="time" id="horaini" class="form-control"  >
	  </div>
	  <div class="form-group">
			<label>End time:</label>
			<input type="date" id="diafin" class="form-control" >
			<input type="time" id="horafin" class="form-control" >
	  </div>

		<!-- LeafLetJs MAP OpenSource -->
		<div class="form-group">
			<button type="button"  class="btn btn-primary"  data-toggle="collapse" data-target="#demo">Add Location</button>
		  <div id="demo" class="collapse">
			<br>
			 <h5>Select a location!</h5>
			 		<span>The presentation will be only accesible 100 meters around mark.</span><span id="clearmark" style="color:#0645AD"> Clear marker</span>
					<!--map div-->
					<div id="mapid"></div>
					<style type="text/css">
					  #mapid{ height: 400px; }
					</style>
					<input type="text" id="lat"  readonly="yes" style="display:none">
					<input type="text" id="lng" readonly="yes" style="display:none">
		  </div>
	  </div>

		<!-- FIIIN    LeafLetJs MAP OpenSource -->

		<button type="button" class="btn btn-primary clone" id="mylove" data-toggle="collapse" data-target="#demo1">Add Survey 1</button>
		<p id="mylove2"></p>
		<!-- Template for add surveys -->
		<script type="text/html" id="add-survey-template">
				<div class="form-group" >
							<div id="demo%%NUMS%%" class="collapse">
								<div class="form-group">
									<label>Question:</label>
									<input type="text" id="question%%COUNT%%" class="form-control" name="question[]" value="Question">
								</div>
							
								<div class="form-group">
									<label>Page number:</label>
									<input type="number" id="page%%COUNT%%" name="page[]" class="form-control" value="0">
								</div>
								<div class="form-group">
									<label>Position X and Y (px):</label>
									<input type="number" id="xcor%%COUNT%%" class="form-control" name="xcor[]" value="0">
									<input type="number" id="ycor%%COUNT%%" class="form-control" name="ycor[]" value="0">	
								</div>
								<div class="form-group">
									<label>Width and Height (px)</label>
									<input type="number" id="width%%COUNT%%" class="form-control" name="width[]" value="50">
									<input type="number" id="height%%COUNT%%" class="form-control" name="height[]" value="50">	
								</div>
								<div class="checkbox">
									<label><input id='multiplechoice%%COUNT%%' type="checkbox" name="multiplechoice[]"> Multiple choice allowed?</label>
								</div>
								<button type="button"  class="btn btn-secondary"  data-toggle="collapse" data-target="#demo%%NUMPLUS%%">Add Answers</button>
								<div class="form-group">
									<div id="demo%%NUMPLUS%%" class="collapse">
											<div class="form-group">
												<label>Answer1:</label>
												<input type="text" id="answer%%SUBNUMS%%" name="answer1[]" class="form-control" >
											</div>
											<div class="form-group">
												<label>Answer2:</label>
												<input type="text"  id="answer%%SUBNUMS%%" name="answer2[]" class="form-control" >
											</div>
											<div class="form-group">
												<label>Answer3:</label>
												<input type="text"   id="answer%%SUBNUMS%%" name="answer3[]" class="form-control" >
											</div>
												<div class="form-group">
												<label>Answer4:</label>
												<input type="text"  id="answer%%SUBNUMS%%" name="answer4[]" class="form-control" >
											</div>
											<div class="form-group">
												<label>Answer5:</label>
												<input type="text" id="answer%%SUBNUMS%%" name="answer5[]" class="form-control" >
											</div>
									</div>
								</div>
							</div>
				</div>
				<button type="button" class="btn btn-primary clone" id="mylove" data-toggle="collapse" data-target="#demo%%NUMS%%">Add Survey %%COUNT%%</button>
				<p id="mylove2"></p>
			</script>
	
	</form> 

	  
	  <div class="mt-2">
		<a href="#" class="btn btn-success" id="btnApiStart">
			<i class="fa fa-play"></i> Submit
		</a>
		<!--<a href="#" class="btn btn-danger" id="btnApiCancel">
			<i class="fa fa-stop"></i> Stop
		</a>-->
		<a href="#" class="btn btn-danger" id="btnApiReset">
			<i class="fa fa-stop"></i> Reset
		</a>
	  </div>

 <!-- <div class="row">
-        <div class="col-12">
-           <div class="card h-100">
-            <div class="card-header">
-              Debug Messages
-            </div>
-
-            <ul class="list-group list-group-flush" id="debug">
-              <li class="list-group-item text-muted empty">Loading plugin....</li>
-            </ul>
-          </div>
-        </div>
-      </div> <!-- /debug -->

</main>