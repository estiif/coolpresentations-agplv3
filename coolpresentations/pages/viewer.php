<main role="main" class="container-fluid" id='window' >
<?php if($presentation === null): ?>
    <div class="row justify-content-center">
        <div class="alert alert-danger col-lg-7 col-xl-5 col-xs-12">
            <?php echo $error ?>
        </div>
    </div>
<?php else: ?>
    <div class="row justify-content-center">
        <div id="over-slides" class="resize-container" ></div>
        <div class="col text-center">
            <!--<canvas id="the-canvas" ></canvas>-->
            <canvas id="the-canvas" class="canvaswidth"></canvas>
            <canvas class="rainbow-pixel-canvas" id="second-canvas" ></canvas>
        </div>
    </div>
    <div id="pages">
      <div class="row presentation-controls justify-content-center">
          <button class="btn btn-outline-primary"  id="prev"><i class="fas fa-arrow-circle-left"></i></button> &nbsp;
          <button class="btn btn-outline-primary"  id="next"><i class="fas fa-arrow-circle-right"></i></button>
      </div>
      <div class="row presentation-controls justify-content-center">
      <div id="marcadorPag"><span><span id="page_num"></span> / <span id="page_count"></span></span></div>
      </div>
    </div>
    <!-- end of presentation -->
  
    <div class="row presentation-controls justify-content-center display-none" id="display-none"  style="height:100% !important">
      <!--<div class="btn-group-vertical justify-content-center" style="width:50%">
           <button type="button" class="end btn-sq-lg btn-block" data-toggle="modal" data-target="#DoubtsModal" id="doubtsbutton"><i class="fa fa-star" ></i> Doubt reminder</button>
           <p></p>
           <button type="button" class="end btn-sq-lg btn-block" data-toggle="modal" data-target="#ResultsModal" id="resultsbutton"><i class="fa fa-star" ></i> Final results</button>
            <p></p>
           <button type="button" class="end btn-sq-lg btn-block" data-toggle="modal" data-target="#RatingModal" id="ratingbutton"><i class="fa fa-star" ></i> Rate presentaion</button>
      </div>-->
      <div class="btn-group justify-content-center" style="height:100% !important" >
          <section class="firstsect">
            <button type="button" style="background: transparent" data-toggle="modal" data-target="#DoubtsModal" id="doubtsbutton"><i class="fa fa-star" ></i> Doubt reminder</button>
          </section>  
          <section class="secondsect">
            <button type="button" class="" data-toggle="modal" data-target="#ResultsModal" id="resultsbutton"><i class="fa fa-star" ></i> Final results</button>
          </section>
          <section class="thirdsect">
            <button type="button" class="" data-toggle="modal" data-target="#RatingModal" id="ratingbutton"><i class="fa fa-star" ></i> Rate presentaion</button>
          </section>
      </div>
    </div>


    <!-- Button trigger modal add questions -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" id="dudabutton" hidden>
      Add question
    </button>
 
    
    <div id="passwordModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Password</h4>
            </div>
            <div class="modal-body">
            <div class="alert alert-danger" style="display:none" id="pass_dialog_fail" role="alert">
                Incorrect code.
            </div>
            <div class="loader" style="margin: auto; display: none"></div>
            <form class="form-horizontal" id="pwd_form" >             
                <div class="form-group">
                </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                        <input type="password" class="form-control" name="pwd_field" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" name="back" href=".">Back</a>
                    <button type="button" class="btn btn-primary" name="send_pwd">Send</button> 
                </div>
                </div>
            </form>
        </div>
    </div>		

    <!-- Survey template -->
    <script type="text/html" id="survey-template">
      <div class="resize-drag" >
              <form class="survey" id="survey">
                  <div id="removeSurvey" onclick="parentNode.remove()"><i class="fas fa-times"></i></div>
                  <div class="row">
                      <h4>%%QUESTION%%</h4>
                  </div>
                  <div class="row">
                      <ul class="list-group">
                          %%ANSWERS%%
                      </ul>
                  </div>
                  <div class="row" id="vote-button">
                      <button type="button" class="btn btn-success btn-block">Vote</button>
                  </div>
              </form>
      </div>
    </script>
 
    <!-- Survey answer template -->
    <script type="text/html" id="survey-answer-template">
            <li data-value="%%VALUE%%" class="list-group-item">%%TEXT%%</li>
    </script>
    <!-- Voted survey answer template -->
    <script type="text/html" id="voted-survey-answer-template">
            <li data-value="%%VALUE%%" class="list-group-item">
                %%TEXT%% <br>
                <!--<span class="votes">%%VOTES%%</span> votes-->
                <div class="progress bg-dark">
                    <div class="progress-bar" role="progressbar" aria-valuenow="%%PERCENTAGE%%" aria-valuemin="0" aria-valuemax="100">%%PERCENTAGE%%%</div>
                </div>
            </li>
    </script>

<!--DAVID-->
<div class="btn-group btn-group-toggle" data-toggle="buttons" id="option-survey">
  <label class="btn btn-outline-secondary btn-success" id="draw-opt">
    <input type="radio" name="options" id="draw-option" autocomplete="off" value="1" checked> Doubt
  </label>
  <label class="btn btn-outline-secondary btn-success active" id="drag-opt">
    <input type="radio" name="options" id="drag-option" autocomplete="off" value="2"> Vote
  </label>
</div>
<div>
  <button type="button" class="btn btn-primary" id="fullscreen-mobile"  style="display:none"><i class="fas fa-expand"></i></button>
</div>
<!--FIN DAVID-->



<!-- Doubts Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">What is your doubt?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="doubtform" >
            <div class="form-group">
                <label for="message-text" class="col-form-label">Message:</label>
                <textarea class="form-control" id="message-text" name="message-text"></textarea>
                <input type="text" id="pageform" name="pageform" hidden >
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="guardar">Save</button>
      </div>
    </div>
  </div>
</div>

<!--Satisfaction Survey -->
<div class="modal fade" id="RatingModal" tabindex="-1" role="dialog" aria-labelledby="RatingModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" id="RatingModal">
      <div class="modal-header">
        <h5 class="modal-title" id="feedback-title">Thanks for rating :)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="ratingform" >
            <div class="form-group">
                <label for="message-text2" class="col-form-label">Usefulness of the content presented</label>
                <div class="star-rating uno">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
                <label for="message-text2" class="col-form-label">Organization of the content</label>
                <div class="star-rating dos">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
                <label for="message-text2" class="col-form-label">Presentator skills</label>
                <div class="star-rating tres">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
                <label for="message-text2" class="col-form-label">Usefulness of the surveys</label>
                <div class="star-rating cuatro">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
                <label for="message-text2" class="col-form-label">General satisfaction</label>
                <div class="star-rating cinco">
                    <span class="fa fa-star" data-rating="1"></span>
                    <span class="fa fa-star" data-rating="2"></span>
                    <span class="fa fa-star" data-rating="3"></span>
                    <span class="fa fa-star" data-rating="4"></span>
                    <span class="fa fa-star" data-rating="5"></span>
                    <input type="hidden" name="whatever1" class="rating-value" value="0">
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary mr-auto" id="feedsubmit">Submit</button>
      </div>
      </div>
      </div>
      </div>

      <!------------- Modal RESULTS ---------->
      <div class="modal fade" id="ResultsModal" tabindex="-1" role="dialog" aria-labelledby="ResultsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content" id="ResultsModal">
           <div class="modal-header">
             <h5 class="modal-title text-center" id="feedback-title">Survey results</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
           </div>     
          <div class="modal-body">
                <div class="accordion" id="accordionExample">
                  
                </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <script type="text/html" id="results-questions-template">
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#%%IDENTIFICADORES%%" aria-expanded="true" aria-controls="collapseOne">           
                          %%QUESTION%%
                        </button>
                      </h2>
                    </div>
                  </div>
                  <div id="%%IDENTIFICADORES%%" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">

                  </div>
      </script>
      <script type="text/html" id="results-answers-template">
                      <div class="card-body">
                        <span>%%ANSWERS%%</span>
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="%%PERCENTAGE%%" aria-valuemin="0" aria-valuemax="100">
                          %%PERCENTAGE%%%
                          </div>
                        </div>
                      </div>
      </script>

      <!------------ Modal Doubt Reminder ---------> 
      <div class="modal fade" id="DoubtsModal" tabindex="-1" role="dialog" aria-labelledby="DoubtsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content" id="DoubtsModal">
           <div class="modal-header">
             <h5 class="modal-title text-center" id="feedback-title">Doubt reminder</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
           </div>     
          <div class="modal-body">
            <ul class="list-group dynamically">

            </ul>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <script type="text/html" id="results-doubts-template">
      <li class="list-group-item list-group-item-action list-group-item-%%CLASES%%">Page: %%PAGES%%->%%DOUBTS%% </li>
    </script>


 


<?php endif; ?>

</main>