<?php if($presentation !== null): ?>
    <?php
        $jsonPolls = json_encode($polls, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        if($jsonPolls === false) {
            $jsonPolls = json_encode(array("jsonError", json_last_error_msg()));
        }
        $jsonPres = json_encode($presentation, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        if($jsonPres === false) {
            $jsonPres = json_encode(array("jsonError", json_last_error_msg()));
        }
        
    ?>
   
    <script src="js/sha.js"></script>
   
    <script src="js/presentationControl.js"></script>
    <script language="JavaScript" type="text/javascript">
        const PRES = <?php echo $jsonPres; ?>;
        const SURVEYS = <?php echo $jsonPolls; ?>;
        const PRES_CODE = "<?php echo $presentation['id_code']; ?>";
        const PRESENTATOR_MODE = <?php echo (isset($_SESSION['user_id'])&&$_SESSION['user_id']===$presentation['author']?'true':'false');?>;
    </script>
    <script src="js/interact.js"></script>
    <script src="js/draggable.js"></script>
    <script src="js/drawable.js"></script>
    <!--Submit modal form -->
    <script>

            function gup(name){
                name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                var regexS = "[\\?&]"+name+"=([^&#]*)";
                var regex = new RegExp(regexS);
                var results = regex.exec(window.location.href);
                if (results == null) {
                    return '';
                } else {
                    return results[1];
                }
            }

    </script>
    <script src="js/doubts.js"></script>
    <script src="js/rating.js"></script>



<?php endif; ?>