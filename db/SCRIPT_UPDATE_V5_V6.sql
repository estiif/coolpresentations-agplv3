-- Select the database
USE database_1;

-- Insert version
UPDATE `table_version` SET `version`=5 WHERE `version`=4;

-- Add feedback to presentations
ALTER TABLE `presentations` ADD 'firstfeed' INT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `presentations` ADD 'secondfeed' INT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `presentations` ADD `thirdfeed' INT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `presentations` ADD 'fourthfeed' INT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `presentations` ADD 'fifthfeed' INT UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `presentations` ADD 'votes' INT UNSIGNED NOT NULL DEFAULT 0;

